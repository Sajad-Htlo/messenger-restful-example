A restful web service example with `Jersey` demonstrates something like facebook resources.

Resources: Profiles , Messages , Comments

For example, by URL `http://localhost:8080/messages/2/comments/` you got all the comments of message with id=2

Technologies used:

Web service: JAX-RS implementation with`Jersey`

`Maven`

`Tomcat`

I used hash tables and sets instead of Database for simplicity.