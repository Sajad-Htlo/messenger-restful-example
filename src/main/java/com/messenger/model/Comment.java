package com.messenger.model;

import java.util.Date;

public class Comment {

    private long id;
    private String message;
    private Date created;
    private String author;

    public Comment() {
    }

    public Comment(long id, String message, String author) {
        this.setId(id);
        this.setMessage(message);
        this.setAuthor(author);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "comment, id: " + id + " , content: " + message + " , author: " + author;
    }
}
