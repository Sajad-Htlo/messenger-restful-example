package com.messenger.resources;

import com.messenger.model.Profile;
import com.messenger.service.ProfileService;

import javax.ws.rs.*;
import java.util.List;

@Path("/profiles")
@Consumes("application/json")
@Produces("application/json")
public class ProfileResource {

    private ProfileService profileService = new ProfileService();

    @GET
    public List<Profile> getProfiles() {
        return profileService.getAllProfiles();
    }

    @GET
    @Path("/{profileN}")
    public Profile getProfile(@PathParam("profileN") String profileName) {
        return profileService.getProfile(profileName);
    }

    @POST
    public Profile addProfile(Profile newProfile) {
        profileService.addProfile(newProfile);
        return newProfile;
    }

    @PUT
    @Path("/{pName}")
    public Profile updateProfile(@PathParam("pName") String profileName, Profile profile) {
        profile.setProfileName(profileName);
        return profileService.updateProfile(profile);
    }

    @DELETE
    @Path("/{pName}")
    public void deleteProfile(@PathParam("pName") String profileName) {
        profileService.removeProfile(profileName);
    }
}
