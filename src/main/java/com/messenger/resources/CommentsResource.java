package com.messenger.resources;

import com.messenger.model.Comment;
import com.messenger.service.CommentService;

import javax.ws.rs.*;
import java.util.List;

@Path("/")
@Produces("application/json")
@Consumes("application/json")
public class CommentsResource {

    private CommentService commentService = new CommentService();

//    @GET
//    @Path("/{cID}")
//    public String getSubSrc(@PathParam("msgId") long messageId, @PathParam("cID") long commentId) {
//        return "comments sub src , messageId: " + messageId + " , commentId: " + commentId;
//    }

    @GET
    public List<Comment> getAllComments(@PathParam("msgId") long messageId) {
        return commentService.getAllComments(messageId);
    }

    @GET
    @Path("/{cmntId}")
    public Comment getComment(@PathParam("msgId") long messageId, @PathParam("cmntId") long commentId) {
        return commentService.getComment(messageId, commentId);
    }

    @POST
    public Comment addNewComment(@PathParam("msgId") long messageId, Comment newComment) {
        System.out.println("add a new comment to messageId: " + messageId);
        commentService.addComment(messageId, newComment);
        return newComment;
    }

}
