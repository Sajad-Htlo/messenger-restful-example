package com.messenger.resources;

import com.messenger.model.Message;
import com.messenger.service.MessageService;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

@Path("/messages")
@Consumes("application/json")
@Produces("application/json")
public class MessageResource {

    MessageService messageService = new MessageService();

    @GET
    public List<Message> getMessages(@QueryParam("year") int year,  //@QueryParam is `?param`
                                     @QueryParam("start") int start,
                                     @QueryParam("size") int size) {

        if (year > 0) {
            return messageService.getAllMessagesByYear(year);
        }
        if (start >= 0 && size > 0) {
            return messageService.getAllMessagesPaginated(start, size);
        }
        return messageService.getAllMessages();
    }

    @GET
    @Path("/{msgID}")
    public Message getMessage(@PathParam("msgID") long myParam) { // @PathParam is `/param`
        return messageService.getMessage(myParam);
    }

    @POST
    public Response addMessage(Message message, @Context UriInfo uriInfo) {

        Message newMessage = messageService.addMessage(message);
        String newMessageId = String.valueOf(newMessage.getId());
        URI uri = uriInfo.getAbsolutePathBuilder().path(newMessageId).build();
        return Response.created(uri).entity(newMessage).build();
    }

    @PUT
    @Path("/{msgID}")
    public Message updateMessage(@PathParam("msgID") long id, Message message) {
        message.setId(id);
        return messageService.updateMessages(message);
    }

    @DELETE
    @Path("/{idToDelete}")
    public void deleteMessage(@PathParam("idToDelete") long id) {
        messageService.deleteMessage(id);
    }

    @Path("/{msgId}/comments")
    public CommentsResource getCommentsResource() {
        return new CommentsResource();
    }
}
