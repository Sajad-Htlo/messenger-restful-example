package com.messenger.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

@Path("/test")
@Produces("text/plain")
@Consumes("text/plain")
public class TestResource {

    @GET
    @Path("/myAnnotations")
    public String getParamsUsingAnnotations(@MatrixParam("mtp") String myMatrixParam,
                                            @HeaderParam("myHeader") String hdr,
                                            @CookieParam("myCookie") String coke) {
        return "Hello Test, matrix param: " + myMatrixParam + " , header: " + hdr + " , cookie: " + coke;
    }

    @GET
    @Path("/myContext")
    public String getParamsUsingContext(@Context UriInfo uriInfo, @Context HttpHeaders headers) {

        String path = uriInfo.getAbsolutePath().toString();
        String myHeader = headers.getRequestHeader("hh").get(0);
        return "context, uri path: " + path + " , header value: " + myHeader;
    }
}
