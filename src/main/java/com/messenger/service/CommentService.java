package com.messenger.service;

import com.messenger.database.DatabaseClass;
import com.messenger.model.Comment;
import com.messenger.model.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommentService {

    private Map<Long, Message> messages = DatabaseClass.getMessageMap();

    public List<Comment> getAllComments(long messageId) {
        Map<Long, Comment> allComments = messages.get(messageId).getComments();
        return new ArrayList<Comment>(allComments.values());
    }

    public Comment getComment(long msgId, long commentId) {
        return messages.get(msgId).getComments().get(commentId);
    }

    public Comment addComment(long msgId, Comment newComment) {
        Map<Long, Comment> allCommentsOfAMessage = messages.get(msgId).getComments();

        newComment.setId(allCommentsOfAMessage.size() + 1);
        System.out.println("allCommentsOfAMessage before adding new comment: " + allCommentsOfAMessage);

        allCommentsOfAMessage.put(newComment.getId(), newComment);
        System.out.println("after adding, " + allCommentsOfAMessage);


        // i should update the message object with give msgId and then update it's comments
        MessageService messageService = new MessageService();

        Message ownerOfComments = messageService.getMessage(msgId); // ok
        System.out.println("ownerOfComments: " + ownerOfComments);

        ownerOfComments.setComments(allCommentsOfAMessage);

        messageService.updateMessages(ownerOfComments);

        return newComment;
    }

}
