package com.messenger.service;

import com.messenger.database.DatabaseClass;
import com.messenger.exception.DataNotFoundException;
import com.messenger.model.Comment;
import com.messenger.model.Message;

import java.util.*;

public class MessageService {

    private static Map<Long, Message> messages = DatabaseClass.getMessageMap();

    public MessageService() {
        messages.put(1l, new Message(1, "my message", new Date(), "sajjad", new Comment(1l, "cmnt1", "aut1")));
        messages.put(2l, new Message(2, "your message", new Date(), "hamed", new Comment(2l, "cmnt2", "aut2")));
    }

    public List<Message> getAllMessages() {
        return new ArrayList<Message>(messages.values());
    }

    public List<Message> getAllMessagesByYear(int year) {

        List<Message> messagesToReturn = new ArrayList<Message>();
        Calendar cal = Calendar.getInstance();

        for (Message eachMessage : messages.values()) {
            if (cal.get(Calendar.YEAR) == year) {
                messagesToReturn.add(eachMessage);
            }
        }
        return messagesToReturn;
    }

    public List<Message> getAllMessagesPaginated(int start, int size) {
        List<Message> list = new ArrayList<Message>(messages.values());

        if ((start + size) > list.size()) {
            return new ArrayList<Message>();
        }
        return list.subList(start, start + size);
    }

    public Message getMessage(long id) {
        Message foundMessage = messages.get(id);
        if (foundMessage == null) {
            throw new DataNotFoundException("Message id: " + id + " not found.");
        }
        return foundMessage;
    }

    public Message addMessage(Message message) {
        message.setId(messages.size() + 1);
        messages.put(message.getId(), message);
        return message;
    }

    public Message updateMessages(Message message) {
        if (message.getId() <= 0) {
            return null;
        }
        messages.put(message.getId(), message);
        return message;
    }

    public void deleteMessage(long id) {
        messages.remove(id);
    }


}
