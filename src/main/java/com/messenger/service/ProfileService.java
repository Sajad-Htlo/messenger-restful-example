package com.messenger.service;

import com.messenger.database.DatabaseClass;
import com.messenger.model.Profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProfileService {


    private Map<String, Profile> profiles = DatabaseClass.getProfileMap();

    public ProfileService() {
        profiles.put("sajjad", new Profile(1l, "sajjad", "sajjad", "htlo"));
        profiles.put("hamed", new Profile(2l, "hamed", "hamed", "mstf"));
    }

    public List<Profile> getAllProfiles() {
        return new ArrayList<Profile>(profiles.values());
    }

    public Profile getProfile(String pName) {
        return profiles.get(pName);
    }

    public Profile addProfile(Profile newProfile) {
        newProfile.setId(profiles.size() + 1);
        profiles.put(newProfile.getProfileName(), newProfile);
        return newProfile;
    }

    public Profile updateProfile(Profile profile) {
        if (profile.getProfileName().isEmpty()) {
            return null;
        }
        profiles.put(profile.getProfileName(), profile);
        return profile;
    }

    public Profile removeProfile(String pName) {
        return profiles.remove(pName);
    }


}
